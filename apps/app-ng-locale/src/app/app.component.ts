import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'ng-locale-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  title = 'app-ng-locale';
}
